#include "StdAfx.h"
#include "Camera.h"

using namespace cv;

bool clicked = false;

const int Camera::Range = 15; 
Point2i Camera::pt = Point2i(-1,-1);
double Camera::Scale = 1;
const int Camera::maxexposure = 7;
const int Camera::maxbrightness = 255;
const int Camera::maxgain = 255;
const int Camera::maxcontrast = 255;
const int Camera::maxsaturation = 255;


Camera::Camera(void)
{
	
}

void Camera::start(void)
{
	cap.open(0);
	if(!cap.isOpened())
	{
		std::cout << "Cannot open the webcam!"; 
	}
	frameHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
	FrameWidth =  cap.get(CV_CAP_PROP_FRAME_WIDTH);
	exposure = cap.get(CV_CAP_PROP_EXPOSURE) + 9;
	brightness = cap.get(CV_CAP_PROP_BRIGHTNESS);
	contrast = cap.get(CV_CAP_PROP_CONTRAST);
	saturation = cap.get(CV_CAP_PROP_SATURATION);
	cap.set(CV_CAP_PROP_GAIN, gain);
	_ftime(&prevtime);

}

void Camera::capture(void)
{
	std::stringstream sst;
	long milliseconds;
	_timeb time;
	_ftime(&time);
	milliseconds = (time.time - prevtime.time)*1000 + (time.millitm - prevtime.millitm);
	prevtime = time;
	if(!cap.read(imageOriginal))
	{
		std::cout << "cannot read a frame from video stream!";
	}
	if(milliseconds > 0)
		sst << 1000/milliseconds << " fps";
	else
		sst << "inf fps";
	putText(imageOriginal, sst.str(), Point(300, 10), 1, 1, Scalar(0, 255, 0));
	sst.str("");
}

void Camera::threshold(Thing t, bool useROI)
{
	if(useROI)	
	{
		Mat temp = imageOriginal(t.ROI);
		cvtColor(temp, imageHSV, COLOR_BGR2HSV);
	}
	else
		cvtColor(imageOriginal, imageHSV, COLOR_BGR2HSV);
	inRange(imageHSV, t.minHSV, t.maxHSV, imageThreshold);
	morphOps(imageThreshold);
	//blur(imageThreshold, imageThreshold, Size(10, 10));
}


void Camera::calibrate(Thing *t, std::vector<Scalar> &minHSVs, std::vector<Scalar> &maxHSVs)
{
	putText(imageOriginal, t->toString(), Point(10, 50), 1, 2, t->colour);
	imshow("Original", imageOriginal);
	setMouseCallback("Original", onMouse, &pt);
	cvtColor(imageOriginal, imageHSV, COLOR_BGR2HSV);
	Scalar minHSV = t->minHSV, maxHSV = t->maxHSV; 
	if(clicked)
	{
		Vec3b pix = imageHSV.at<Vec3b>(pt.y, pt.x);
		for(int i = 0; i < 3; i++)
		{
			minHSV[i] = MIN((int)minHSV[i], (int)pix[i] - Range);
			maxHSV[i] = MAX((int)maxHSV[i], (int)pix[i] + Range);
		}
		minHSVs.push_back(minHSV);
		maxHSVs.push_back(maxHSV);
		clicked = false;
	}
	this->threshold(*t);
	imshow("Thresholded", imageThreshold);
}

void Camera::distortionCalib(int numBoards, int cornersHor, int cornersVer)
{
	int numSquares = cornersHor * cornersVer;
    Size board_sz = Size(cornersHor, cornersVer);
	std::vector<std::vector<Point3f> > object_points;
    std::vector<std::vector<Point2f> > image_points;
	std::vector<Point2f> corners;
    int successes=0;
	Mat image;
    Mat gray_image;
    cap >> image;
	vector<Point3f> obj;
    for(int j=0;j<numSquares;j++)
        obj.push_back(Point3f(j/cornersHor, j%cornersHor, 0.0f));
	while(successes<numBoards)
    {
		cvtColor(image, gray_image, CV_BGR2GRAY);
		bool found = findChessboardCorners(image, board_sz, corners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);

        if(found)
        {
            //cornerSubPix(gray_image, corners, Size(11, 11), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
            drawChessboardCorners(gray_image, board_sz, corners, found);
			image_points.push_back(corners);
            object_points.push_back(obj);
			successes++;
        }
		imshow("win1", image);
        imshow("win2", gray_image);

        cap >> image;
        int key = waitKey(1);
		if(key==27)
            return;
        if(successes>=numBoards)
            break;
    }
	Mat intrinsic = Mat(3, 3, CV_32FC1);
    Mat distCoeffs;
    vector<Mat> rvecs;
    vector<Mat> tvecs;
	intrinsic.ptr<float>(0)[0] = 1;
    intrinsic.ptr<float>(1)[1] = 1;
	calibrateCamera(object_points, image_points, image.size(), intrinsic, distCoeffs, rvecs, tvecs);
	Mat imageUndistorted;
    while(1)
    {
        cap >> image;
        undistort(image, imageUndistorted, intrinsic, distCoeffs);

        imshow("original", image);
        imshow("undistorted", imageUndistorted);
        waitKey(1);
    }
}

void Camera::rescale(Thing *t)
{
	int count = 0;
	double areaSum = 0;
	double avgArea, trueArea;
	Rect roi = t->ROI;
	t->ROI = Rect(0, 0, FrameWidth, frameHeight);
	while(count <= 10)
	{
		this->capture();
		std::vector<Thing> vec = this->locate(*t, true);
		imshow("Rescaling", imageOriginal);
		waitKey(1);
		for(int i = 0; i <vec.size(); i++)
		{	
			areaSum += vec[i].area;
			count++;
		}
	}
	t->ROI = roi;
	avgArea = areaSum/count;
	trueArea = t->getTrueArea();
	Scale *= pow(avgArea/trueArea, 0.5);
	std::cout << Scale << "\n";
	destroyWindow("Rescaling");
}





void Camera::stop()
{
	destroyWindow("Original");
	destroyWindow("Thresholded");
	cap.release();
}

Camera::~Camera(void)
{
}

void morphOps(Mat &image)
{

	Mat erodeElement = getStructuringElement( MORPH_RECT,Size(3,3));
	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement( MORPH_RECT,Size(8,8));
	//morphological opening (remove small objects from the foreground)
  erode(image, image, erodeElement);
  dilate(image, image, dilateElement);  

   //morphological closing (fill small holes in the foreground)
  dilate(image, image, dilateElement); 
  erode(image, image, erodeElement); 
}

void onMouse(int evt, int x, int y, int flags, void* param)
{
	if(evt == CV_EVENT_LBUTTONDOWN)
	{
		Point* p = (Point*)param;
		p->x = x;
		p->y = y;
		clicked = true;
	}
}

