#pragma once
#include "Thing.h"
class Platform :
	public Thing
{
public:

	double width;
	double length;

	Platform(void);
	Platform(cv::Point location);
	Platform(Thing t);
	std::string toString();
	double getTrueArea();
	double getWidth();
	double getHeight();
	double getLength();
	~Platform(void);
};

