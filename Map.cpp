#include "StdAfx.h"
#include "Map.h"

using namespace cv;

const double Map::Scale = 1.5;
const int Map::Width = 420;
const int Map::Height = 300;

Map::Map(void)
{
	this->home = new Platform(Point(Width - 55, Height/2));
	this->home->index = 0;
	this->ship = new Platform(Point(0 + 55, Height/2));
	this->ship->index = 1;
	nrOfLandmarks = 2;
	nrOfCones = 4;
	completed = false;
	allConesSeen = false;
	platformInSight = false;
}


void Map::update(Cone c, StateSpace *ss, Point *robot)
{
		// Match c with known landmarks
	c.location = c.location + *robot;
	for(int i  = 0; i < this->cones.size(); i++)
	{
		if(areSameCones(c, cones[i]))
		{
			cones[i].certainty += 0.2;
			if(cones[i].updated)
			{
				// update ss
				c.index = cones[i].index;
				if(platformInSight)
					ss->map(c);
				else
					ss->measurementUpdate(c);
				//update map
				*robot = ss->getRobotPos();
				cones[i].location = ss->getLandmarkPos(c.index);
				cones[i].inSight =  1;
				return;
			}
			if(!(cones[i].updated) && cones[i].certainty >= 0.5 && !allConesSeen)
			{
				ss->addNewLandmark(c);
				cones[i].index = nrOfLandmarks;
				nrOfLandmarks++;
				cones[i].updated = true;
				return;
			}
			return;
		}
	}
	c.certainty = 0.11;
	this->cones.push_back(c);
}

void Map::update(Platform p, StateSpace *ss, Point *robot)
{
		// Match p with known landmarks
	p.location = p.location + *robot;
	if(isOnPlatform(*(this->home), p.location))
	{
		// update ss
		p.index = this->home->index;
		ss->localize(p);
		//update map
		*robot = ss->getRobotPos();
		platformInSight = true;
	}
	else if(isOnPlatform(*(this->ship), p.location))
	{
		// update ss
		p.index = this->ship->index;
		ss->localize(p);
		//update map
		*robot = ss->getRobotPos();
		nrOfCones = cones.size();
		allConesSeen = true;
		platformInSight = true;
	}
}

void Map::update(Refugee r, StateSpace *ss, Point *robot)
{
	r.location += *robot;
	if(isOnPlatform(*ship, r.location))
	{
		for(int i  = 0; i < this->refugees.size(); i++)
		{
			if(areSameRefugees(r, refugees[i]))
			{
				refugees[i].certainty += 0.2;
				if(refugees[i].updated)
				{
					// update ss
					r.index = refugees[i].index;
					ss->map(r);
					//update map
					//*robot = ss->getRobotPos();
					refugees[i].location = ss->getLandmarkPos(r.index);
					refugees[i].inSight =  1;
					return;
				}
				if(!(refugees[i].updated) && refugees[i].certainty >= 0.5 && !completed)
				{
					ss->addNewLandmark(r);
					refugees[i].index = nrOfLandmarks;
					nrOfLandmarks++;
					refugees[i].updated = true;
					return;
				}
				return;
			}
		}
		std::cout << nrOfLandmarks << "\t" << nrOfCones << std::endl;
		if(nrOfLandmarks < 2 + 5 + nrOfCones)
		{
			r.certainty = 0.11;
			this->refugees.push_back(r);
		}
		else
			completed = true;
	}
}

Mat Map::draw(Point robot)
{
	Mat ret(Scale*Height, Scale*Width, CV_8UC3);
	std::stringstream sst;
	rectangle(ret, Point(0, 0), Point(Scale*Width, Scale*Height), Scalar(0, 0, 0), CV_FILLED);
	drawRect(ret, home->location, home->length, home->width, home->colour, CV_FILLED);
	sst << home->location.x << " - " << home->location.y;
	putText(ret, sst.str(), Scale*home->location, 0, 0.3, Scalar(255, 0, 0));
	sst.str("");

	drawRect(ret, ship->location, ship->length, ship->width, ship->colour, CV_FILLED);
	sst << ship->location.x << " - " << ship->location.y;
	putText(ret, sst.str(), Scale*ship->location, 0, 0.3, Scalar(255, 0, 0));
	sst.str("");
	circle(ret, Scale*robot, 15, Scalar(0, 255, 0), CV_FILLED);
	sst << robot.x << " - " << robot.y;
	putText(ret, sst.str(), Scale*robot, 0, 0.3, Scalar(255, 0, 0));
	sst.str("");
	for(int i = 0; i < cones.size();i++)
	{
		drawRect(ret, cones[i].location, cones[i].length, cones[i].width, cones[i].inSight*cones[i].colour, CV_FILLED);
		sst << cones[i].location.x << " - " << cones[i].location.y;
		putText(ret, sst.str(), Scale*cones[i].location, 0, 0.3, Scalar(255, 0, 0));
		sst.str("");
	}
	for(int i = 0; i < refugees.size(); i++)
	{
		circle(ret, Scale*refugees[i].location, Scale*refugees[i].diameter/2,  refugees[i].inSight*refugees[i].colour, CV_FILLED);
		sst << refugees[i].location.x << " - " << refugees[i].location.y;
		putText(ret, sst.str(), Scale*refugees[i].location, 0, 0.3, Scalar(0, 0, 255));
		sst.str("");
	}
	drawRect(ret, robot, 640/Camera::Scale, 480/Camera::Scale, Scalar(0, 255, 255));
	return ret;
}

Mat Map::draw(StateSpace ss)
{
	Mat ret(Height, Width, CV_8UC3);
	Scalar color(0, 255, 0);
	std::stringstream sst;
	Point o(-1, -1);
	rectangle(ret, Point(0, 0), Point(Scale*Width, Scale*Height), Scalar(0, 0, 0), CV_FILLED);
	for(int i = -1; i < nrOfLandmarks; i++)
	{
		if(i >= 0)
			color = Scalar(255, 255, 255);
		if(i > 1)
		{
			color = Scalar(0, 0, 255);
		}
		if(i > nrOfCones + 1)
			color = Scalar(255, 0, 0);
		ellipse(ret, ss.getErrorEllipse(i), color, 1);
		o.x = ss.y(2 + 2*i);
		o.y = ss.y(3 + 2*i);
	}
	drawRect(ret, ss.getRobotPos(), 640/Camera::Scale, 480/Camera::Scale, Scalar(0, 255, 255));
	return ret;
}

void Map::clearFalseCones(void)
{
	std::vector<Cone>::iterator it = cones.begin();
	while(it != cones.end())
	{
		if(it->certainty <= 0)
			it = this->cones.erase(it);
		else
			it++;
	}
}

void Map::manageCones(StateSpace *ss)
{
	std::vector<Cone>::iterator it = cones.begin();
	while(it != cones.end())
	{
		it->inSight = 0.5;
		if(it->location.x < 130 || it->location.x > 290)
			it->certainty = 0;

		if(!(it->updated))
			it->certainty -= 0.05;

		if(it->certainty <= 0)
		{
			if(it->updated)
			{
				nrOfLandmarks--;
				ss->removeLandmark(it->index);
			}
			it = this->cones.erase(it);
			
		}
		else
			it++;
	}
}

void Map::manageRefugees(StateSpace *ss)
{
	std::vector<Refugee>::iterator it = refugees.begin();
	while(it != refugees.end())
	{
		it->inSight = 0.5;
		if(!isOnPlatform(*ship, it->location))
			it->certainty = 0;
		if(!(it->updated))
			it->certainty -= 0.05;

		if(it->certainty <= 0)
		{
			if(it->updated)
			{
				nrOfLandmarks--;
				ss->removeLandmark(it->index);
			}
			it = this->refugees.erase(it);
		}
		else
			it++;
	}
}

void Map::reset(void)
{
	cones.clear();
	refugees.clear();
}

Map::~Map(void)
{
}

bool isOnPlatform(Platform platform, Point p)
{
	bool x, y;
	x = p.x < platform.location.x + platform.length/2 && p.x > platform.location.x - platform.length/2;
	y = p.y < platform.location.y + platform.width/2 && p.y > platform.location.y - platform.width/2;
	return x && y;
}

bool areSameCones(Cone c1, Cone c2)
{
	double x, y, dist;
	x = c1.location.x - c2.location.x;
	y = c1.location.y - c2.location.y;
	dist = std::pow(x, 2) + std::pow(y,2 );
	return dist < std::pow(50.0,2);
}

bool areSameRefugees(Refugee r1, Refugee r2)
{
	double x, y;
	x = std::abs(r1.location.x - r2.location.x);
	y = std::abs(r1.location.y - r2.location.y);
	return x < 1.3*r1.getLength() && y < 1.3*r1.getWidth();
}

void drawRect(Mat &image, Point center,int height, int width, Scalar &color, int thickness, int lineType)
{
	Point p1, p2;
	p1 = Map::Scale*center - Map::Scale*Point(height/2, width/2);
	p2 = Map::Scale*center + Map::Scale*Point(height/2, width/2);
	rectangle(image, p1, p2, color, thickness, lineType);
}

