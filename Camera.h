#pragma once

#include <math.h>

#include "opencv2\opencv.hpp"
#include "Thing.h"
#include "Platform.h"
#include <sys/timeb.h>

using namespace cv;

class Camera
{
public:
	
	static const int Range;
	static cv::Point2i pt;
	static double Scale;
	static const int maxexposure, maxbrightness, maxcontrast, maxgain, maxsaturation;
	

	cv::Point location;
	cv::VideoCapture cap;
	cv::Mat imageOriginal;
	cv::Mat imageHSV;
	cv::Mat imageThreshold;
	std::vector< std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	double frameHeight;
	double FrameWidth;
	_timeb prevtime;
	int exposure, brightness, contrast, gain, saturation;
	

	Camera(void);
	void start(void);
	void capture(void);
	void threshold(Thing t, bool useROI = false);
	template<class T>std::vector<T> locate(T t, bool draw = false, bool correctX = false, bool correctY = false);
	void calibrate(Thing *t, std::vector<cv::Scalar> &minHSVs, std::vector<cv::Scalar> &maxHSVs);
	void distortionCalib(int numBoards, int cornersHor, int cornersVer);
	void rescale(Thing *t);
	void stop();
	~Camera(void);
};

void morphOps(cv::Mat &image);
void onMouse(int evt, int x, int y, int flags, void* param);

template<class T>
std::vector<T> Camera::locate(T t, bool draw, bool correctX, bool correctY)
{
	std::vector<T> things;
	if(t.ROI.area() <= 0)
		return things;
	double fromEdge, sign = 1;
	this->threshold(t, true);
	cv::findContours(imageThreshold , contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	int numOfThings = hierarchy.size();
	if(numOfThings > 0)
	{
		int index = 0;
		cv::Moments moment;
		double area;
		while(index >= 0)
		{
			moment = cv::moments((Mat)contours[index]);
			area = moment.m00;
			if(area > t.minSize && area < t.maxSize)
			{

				T newThing;
				newThing.area = area/(Scale*Scale);
				newThing.location.x = (1/Scale)*(t.ROI.x + moment.m10/moment.m00 - FrameWidth/2);
				newThing.location.y = (1/Scale)*(t.ROI.y + moment.m01/moment.m00 - frameHeight/2);
				newThing.colour = t.colour;
				if(correctX)
				{
					fromEdge = FrameWidth/(2*Scale) - abs(newThing.location.x);
					fromEdge -= newThing.height*abs(newThing.location.x)/(2*210 - newThing.height);
					if(fromEdge < newThing.getLength()/2)
					{
						
						sign = newThing.location.x < 0 ? -1.5 : 1.5;
						newThing.location.x += sign*(newThing.getLength()/2 - fromEdge);
					}
				}
				if(correctY)
				{
					fromEdge = frameHeight/(2*Scale) - abs(newThing.location.y);
					if(fromEdge < newThing.getWidth()/2)
					{
						sign = newThing.location.y < 0 ? -1 : 1;
						newThing.location.y += sign*(newThing.getWidth()/2 - fromEdge);
					}
				}
				
				things.push_back(newThing);	
			}
			index = hierarchy[index][0];
		}
		if(draw)
		{
			drawThings(things, imageOriginal, contours, hierarchy, Point(t.ROI.x, t.ROI.y));
			
		}
	}
	cv::rectangle(imageOriginal, t.ROI, t.colour);
	return things;
}
template<class T>
void drawThings(std::vector<T> things, cv::Mat &frame, std::vector< vector<cv::Point> > contours, std::vector<Vec4i> hierarchy, cv::Point offset)
{
	std::stringstream sst;
	for(int i = 0; i < things.size(); i++)
	{
		sst.str("");
		cv::drawContours(frame, contours, i, things[i].colour, 3, 8, hierarchy, 2147483647, offset);
		cv::circle(frame, things[i].location*Camera::Scale + Point(frame.cols/2, frame.rows/2), 4, Scalar(0, 0, 0));
		sst << things[i].area;
		cv::putText(frame, sst.str(), things[i].location*Camera::Scale + Point(frame.cols/2, frame.rows/2), CV_FONT_BLACK, 1, Scalar(255, 0, 0));
	}
}

