
#######################
#Uncomment correct LIBX
#######################

#32-bit system xLib
#LIBX = /usr/lib/i386-linux-gnu/libX11.so.6 /usr/lib/i386-linux-gnu/libXext.so.6

#64-bit system xLib
LIBX = /usr/lib/x86_64-linux-gnu/libX11.so.6 /usr/lib/x86_64-linux-gnu/libXext.so.6

##############
#RakNet dir
##############

RAKNETDIR = /home/antti/Desktop/RakNet-master

##################
# Boost location #
##################

# Use the directory that *contains* the boost directory
#BOOST_HEADERS = /usr/include/boost
#BOOST_LIBRARIES = /usr/lib/x86_64-linux-gnu/
# The flavour is the suffix in the library name
# For example: if the library is libboost_filesystem-mt.a, then FLAVOUR is -mt
#BOOST_FLAVOUR =


CXX = g++
CFLAGS = `pkg-config --cflags opencv` -Wall -std=c++11 -I../3rdparty/eigen-eigen-fc3cbb33cb04/ -Iinc -g
LIBS = `pkg-config --libs opencv` -lserial
APP = bin/main
SRCDIR = src/
OBJDIR = obj/

SRC = $(wildcard $(SRCDIR)*.cpp)

OBJS = $(SRC:$(SRCDIR)%.cpp=$(OBJDIR)%.o)

build: $(OBJS)
	$(CXX) $(OBJS) -o $(APP) $(LIBS)

$(OBJDIR)%.o: $(SRCDIR)%.cpp
	$(CXX) $(CFLAGS) -c $(SRCDIR)$*.cpp -o $(OBJDIR)$*.o


clean: cleandoc
	rm -fv $(OBJS)
	rm -fv $(APP)
	
doc:
	doxygen

cleandoc:
	rm -rfv docs/*



