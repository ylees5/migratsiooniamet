#pragma once
#include "Thing.h"
class Refugee :
	public Thing
{
public:

	double diameter;

	Refugee(void);
	Refugee(Thing t);
	std::string toString();
	double getTrueArea();
	double getWidth();
	double getHeight();
	double getLength();
	~Refugee(void);
};

