#include "StdAfx.h"
#include "Platform.h"

using namespace cv;

Platform::Platform(void)
{
	this->minHSV = Scalar(179, 255, 255);
	this->maxHSV = Scalar(0, 0, 0);
	this->colour = Scalar(200, 200, 200);
	this->minSize = 200*200;
	this->maxSize = 10000*10000;
	this->length = 105;
	this->width = 150;
	this->height = 20;
	this->certainty = 0;
	this->index = -1;
	this->ROI = Rect(0, 0, 0, 0);
}
Platform::Platform(cv::Point location)
{
	this->minHSV = Scalar(179, 255, 255);
	this->maxHSV = Scalar(0, 0, 0);
	this->colour = Scalar(200, 200, 200);
	this->minSize = 50*50;
	this->maxSize = 800*800;
	this->length = 105;
	this->width = 150;
	this->height = 20;
	this->location = location;
	this->index = -1;
	this->ROI = Rect(0, 0, 0, 0);


}

Platform::Platform(Thing t)
{
	this->minHSV = t.minHSV;
	this->maxHSV = t.maxHSV;
	this->colour = t.colour;
	this->minSize = t.minSize;
	this->maxSize = t.maxSize;
	this->length = 105;
	this->width = 150;
	this->height = t.height;
	this->certainty = t.certainty;
	this->index = t.index;
	this->ROI = t.ROI;
}

std::string Platform::toString()
{
	return "Platform";
}

double Platform::getTrueArea()
{
	return this->width*this->length;
}


double Platform::getWidth(void)
{
	return width;
}

double Platform::getLength(void)
{
	return length;
}

Platform::~Platform(void)
{
}
