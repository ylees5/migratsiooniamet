#include "StdAfx.h"
#include "GUI.h"

using namespace cv;

GUI::GUI(void)
{
	robot = new Robot();
	namedWindow("GUI", CV_WINDOW_AUTOSIZE);
	//cvCreateButton("Calibrate");


}

void GUI::execute(void)
{
	int keyPressed;
	while(1)
	{
		keyPressed = waitKey(0);
		
		switch(keyPressed)
		{
		case 27:
			return;
		case 99:
			{
				robot->calibrate();
				break;
			}
		case 100:
			{
				robot->camera.start();

				robot->camera.distortionCalib(10, 8, 6);
				break;
			}
		case 114:
			{
				robot->reset();
				std::cout << "reset done \n";
				break;
			}
		case 116:
			{
				robot->test();
				break;
			}
		case 105:
			{
				if(robot->init())
					std::cout << "init succeedd \n";
				else
					std::cout << "init failed \n";
				break;
			}
		case 115:
			{
				robot->start();
				break;
			}
		default:
			{
				if(keyPressed > 0)
			std::cout << keyPressed;
			}
		}	
	}

}

GUI::~GUI(void)
{
}
