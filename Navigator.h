#pragma once

#include <fstream>
#include <Eigen\Dense>
#include "opencv2\opencv.hpp"
#include "Cone.h"


class Navigator
{
public:

	static const int maxAttExp, maxAttGain, maxRepExp, maxRepGain, maxObsRX, maxObsRY, maxTargTol, maxLambda, maxMaxIter;
	static const int minAttExp, minAttGain, minRepExp, minRepGain, minObsRX, minObsRY, minTargTol, minLambda, minMaxIter;
	static const double stepAttExp, stepAttGain, stepRepExp, stepRepGain, stepObsRX, stepObsRY, stepTargTol, stepLambda, stepMaxIter;

	int attractionGain;
	int repulsionGain;
	int repulsionExponent;
	int attractionExponent;
	int obstacleRX;
	int obstacleRY;
	int targetTolerance;
	int lambda;
	std::vector<cv::Point> path;
	int maxIterations;
	

	Navigator(void);
	Eigen::Vector2d getAttractionForce(cv::Point target, cv::Point robot);
	Eigen::Vector2d getRepulsionForce(cv::Point obstacle, cv::Point robot);
	Eigen::Vector2d getResultantForce(cv::Point target, std::vector<Cone> obstacles, cv::Point robot);
	bool planPath(cv::Point target, std::vector<Cone> &obstacles, cv::Point robot, double speed, double dt);
	void drawPath(cv::Mat &image, double scale);
	void setParameters(void);
	void saveParameters(void);
	void readParameters(void);
	~Navigator(void);
};

double distance(cv::Point p1, cv::Point p2);
std::vector<std::string> split(const std::string& s);