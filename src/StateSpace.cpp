
#include "StateSpace.h"

const double StateSpace::k = 25;
const double StateSpace::minR = 2;
const double StateSpace::maxR = 10;
const double StateSpace::chiSquaredVal = 2.4477;
const int StateSpace::MaxSize = 30;
const int StateSpace::RobotPoses = 2;
const int StateSpace::LandmarkPoses = 2;
using namespace Eigen;
using namespace cv;
StateSpace::StateSpace(void)
{
	this->Init();
	y.setZero();
}
StateSpace::StateSpace(Point location, Point home, Point ship)
{
	this->reset(location, home, ship);
}

void StateSpace::predictionUpdate(cv::Point prevPos)
{
	Q << k*abs(prevPos.x - y(0)), 0,
		 0, k*abs(prevPos.y - y(1));
	y(0) = y(0) + (y(0) - prevPos.x);
	if(RobotPoses > 1)
		y(1) = y(1) + (y(1) - prevPos.y);

	P.topLeftCorner(2, 2) = Fy*P.topLeftCorner(2, 2)*Fy.transpose() + Fu*Q*Fu.transpose();
	P.topRightCorner(2, size - 2) = Fy*P.topRightCorner(2, size - 2);
	P.bottomLeftCorner(size - 2, 2) = P.topRightCorner(2, size - 2).transpose();
}

void StateSpace::measurementUpdate(Thing t)
{
	int i = t.index;
	double c = (maxR*(t.getTrueArea() - t.area)/t.getTrueArea()) + minR;
	if(c < minR)
		c = minR;
	this->addMeasurements(t);
	this->predictObservations(t.index);
	R << c, 0,
		 0, c;
	Matrix4d p;
	p.topLeftCorner(2, 2) = P.topLeftCorner(2, 2);
	p.topRightCorner(2, 2) = P.block<2, 2>(0, 2+2*i);
	p.bottomLeftCorner(2, 2) = P.block<2, 2>(2 +2*i, 0);
	p.bottomRightCorner(2, 2) = P.block<2, 2>(2+2*i, 2+2*i);
	IN = H*p*H.transpose() + R;
	MatrixXd p2(MaxSize, 4);
	p2.topLeftCorner(2, 2) = P.topLeftCorner(2, 2);
	p2.topRightCorner(2, 2) = P.block<2, 2>(0, 2+2*i);
	p2.bottomLeftCorner(size - 2, 2) = P.block(2, 0, size-2, 2);
	p2.bottomRightCorner(size - 2, 2) = P.block(2, 2+2*i, size-2, 2);
	K.topLeftCorner(size, 2) = p2.topLeftCorner(size, 4)*H.transpose()*IN.inverse();
	y.head(size) = y.head(size) + K.topLeftCorner(size, 2)*in;
	P.topLeftCorner(size, size) = P.topLeftCorner(size, size) - K.topLeftCorner(size, 2)*IN*K.topLeftCorner(size, 2).transpose();
}

void StateSpace::localize(Thing t)
{
	double c = (maxR*(t.getTrueArea() - t.area)/t.getTrueArea()) + minR;
	if(c < minR)
		c = minR;
	this->addMeasurements(t);
	this->predictObservations(t.index);
	R << c, 0,
		 0, c;
	Matrix2d Prr;
	Prr = P.topLeftCorner(2, 2);
	IN = H.topLeftCorner(2, 2)*Prr*H.topLeftCorner(2, 2).transpose() + R;
	K.topLeftCorner(2, 2) = Prr*H.topLeftCorner(2, 2).transpose()*IN.inverse();
	y.head(2) = y.head(2) + K.topLeftCorner(2, 2) * in;
	P.topLeftCorner(2, 2) = Prr - K.topLeftCorner(2, 2)*IN*K.topLeftCorner(2, 2).transpose();
}

void StateSpace::map(Thing t)
{
	double c = (maxR*(t.getTrueArea() - t.area)/t.getTrueArea()) + minR;
	if(c < minR)
		c = minR;
	this->addMeasurements(t);
	this->predictObservations(t.index);
	R << c, 0,
		 0, c;
	Matrix2d Pll;
	Pll = P.block(2 + 2*t.index, 2 + 2*t.index,2 , 2);
	IN = H.topRightCorner(2, 2)*Pll*H.topRightCorner(2, 2).transpose() + R;
	K.topLeftCorner(2, 2) = Pll*H.topRightCorner(2, 2).transpose()*IN.inverse();
	y.segment(2 + 2*t.index, 2) = y.segment(2 + 2*t.index, 2) + K.topLeftCorner(2, 2)*in;
	P.block(2 + 2*t.index, 2 + 2*t.index, 2, 2) = Pll - K.topLeftCorner(2, 2)*IN*K.topLeftCorner(2, 2).transpose();
}

void StateSpace::predictObservations(int index)
{
	if(size > 2)
	{
		predZ(0) = y(2 + 2*index);
		predZ(1) = y(3 + 2*index);
	}
}

void StateSpace::addMeasurements(Thing t)
{
	Z(0) = t.location.x;
	Z(1) = t.location.y;
	if(t.index >=0)
	{
		predictObservations(t.index);
		in = predZ - Z;
	}
}

void StateSpace::addNewLandmark(Thing t)
{
	Matrix2d Prr;
	double c = (maxR*(t.getTrueArea() - t.area)/t.getTrueArea()) + minR;
	if(c < minR)
		c = minR;
	R << c, 0,
		 0, c;

	y(size) = t.location.x;
	y(size+1) = t.location.y;
	Prr = P.topLeftCorner(2, 2);
	P.block(size, size, 2, 2) = Jxr*Prr*Jxr.transpose() + Jz*R*Jz.transpose();
	P.block(0, size, 2, 2) = Prr*Jxr.transpose();
	P.block(size, 0, 2, 2) = P.block(0, size, 2, 2).transpose();
	P.block(size, 2, 2, size-2) = Jxr*P.block(2, 0, size-2, 2).transpose();
	P.block(2, size, size-2, 2) = P.block(size, 2, 2, size-2).transpose();

	size += LandmarkPoses;
}

void StateSpace::removeLandmark(int index)
{
	for(int i = RobotPoses + LandmarkPoses*(index+1); i < size; i++)
	{
		y(i-LandmarkPoses) = y(i);
		y(i) = 0;
		for(int j = 0; j <= i-LandmarkPoses; j++)
		{
			int from = j;
			if(j >= LandmarkPoses*index + RobotPoses)
				from += LandmarkPoses;
			P(j, i - LandmarkPoses) = P(from, i);
			P(i - LandmarkPoses, j) = P(i, from);
		}
	}
	P.block(0, size-LandmarkPoses, size, LandmarkPoses).setZero();
	P.block(size-LandmarkPoses, 0, LandmarkPoses, size).setZero();
}

cv::Point StateSpace::getLandmarkPos(int index)
{
	return Point(y(2 + 2*index), y(3 + 2*index));
}

cv::Point StateSpace::getRobotPos(void)
{
	return Point(y(0), y(1));
}

void StateSpace::Init(void)
{
	size = RobotPoses + 2*LandmarkPoses;
	Fu.resize(RobotPoses, RobotPoses);
	Fu.setIdentity();
	Fy.resize(RobotPoses, RobotPoses);
	Fy.setIdentity();
	H.resize(LandmarkPoses, LandmarkPoses+RobotPoses);
	R.resize(LandmarkPoses, LandmarkPoses);
	Q.resize(RobotPoses, RobotPoses);
	Q.setIdentity();
	Q *= k;
	IN.resize(LandmarkPoses, LandmarkPoses);
	Jxr.resize(LandmarkPoses, RobotPoses);
	Jxr.setIdentity();
	Jz.resize(LandmarkPoses, LandmarkPoses);
	Jz.setIdentity();
	Z.resize(LandmarkPoses);
	predZ.resize(LandmarkPoses);
	in.resize(LandmarkPoses);
	y.resize(MaxSize);
	P.resize(MaxSize, MaxSize);
	P.setZero();
	//P.topLeftCorner(RobotPoses, RobotPoses).setIdentity();
	//P.topLeftCorner(RobotPoses, RobotPoses) *= 0.5;
	P(0, 0) = 0.5;
	P(1, 1) = 0;
	H.topLeftCorner(RobotPoses, RobotPoses).setIdentity();
	H.topRightCorner(LandmarkPoses, LandmarkPoses).setIdentity();
	H.topRightCorner(LandmarkPoses, LandmarkPoses) *= -1;
	//H << 1, 0, -1, 0,
	//	  0, 1, 0, -1;
	K.resize(MaxSize, 2);
}

void StateSpace::reset(Point location, Point home, Point ship)
{
	this->Init();
	y(0) = location.x;
	y(1) = location.y;
	y(2) = home.x;
	y(3) = home.y;
	y(4) = ship.x;
	y(5) = ship.y;
}

cv::RotatedRect StateSpace::getErrorEllipse(int index)
{
	EigenSolver<Matrix2d> es(P.block(2 + 2*index, 2 + 2*index, 2, 2));
	Vector2cd evals = es.eigenvalues();
	Matrix2cd evecs = es.eigenvectors();
	cv::Point mean(y(2 + 2*index), y(3 + 2*index));
	double angle, a, b;
	if(evals(0).real() > evals(1).real())
	{
		angle = atan2(evecs(0, 1).real(), evecs(0, 0).real());
		a = chiSquaredVal*sqrt(evals(0).real());
		b = chiSquaredVal*sqrt(evals(1).real());
	}
	else
	{
		angle = atan2(evecs(1, 1).real(), evecs(1, 0).real());
		a = chiSquaredVal*sqrt(evals(1).real());
		b = chiSquaredVal*sqrt(evals(0).real());
	}
	if(angle < 0)
		angle += 2*CV_PI;
	angle = 180*angle/CV_PI;
	return cv::RotatedRect(mean, cv::Size2d(a, b), -angle);
}

StateSpace::~StateSpace(void)
{
}
