/* 
 * File:   Arm.cpp
 * Author: icd
 * 
 * Created on November 3, 2015, 9:32 AM
 */

#include "Arm.h"

Arm::Arm() {
}

Arm::Arm(double len1, double len2)
{
    this->l1 = len1;
    this->l2 = len2;
    this->angles[0] = M_PI/4;
    this->angles[1] = M_PI/2;
}

cv::Point Arm::getPosition(void)
{
    int x, y;
    x = l2*std::cos(angles[0] + angles[1]) + l1*std::cos(angles[0]);
    y = l2*std::sin(angles[0] + angles[1]) + l1*std::sin(angles[0]);
    return cv::Point(x, y);
}

void Arm::updateAngles(double a1, double a2)
{
    angles[0] = a1;
    angles[1] = a2;
}



Arm::~Arm() {
}

