
#include "Robot.h"


using namespace cv;

const int Robot::pMultiplier = 1000;
const int Robot::IMultiplier = 10000;


Robot::Robot(void)
{
	theCone = new Cone();
	thePlatform = new Platform();
	theRefugee = new Refugee();
	map = new Map();
	this->location = map->home->location + Point(-Map::Width/2, 0);
	this->prevLocation = this->location;
	this->ss = new StateSpace(this->location, this->map->home->location, this->map->ship->location);
	this->navi = new Navigator();
        this->arm1 = new Arm(10, 10);
        this->arm2 = new Arm(10, 10);
}

void Robot::start(void)
{
	int k;
	camera.start();
	std::vector<Platform> platforms;
	std::vector<Cone> cones;
	std::vector<Refugee> refugees;
	Mat theMap;
	while(1)
	{
		camera.capture();
		setROIs();
		cones = camera.locate(*theCone, true, true, true);
		platforms = camera.locate(*thePlatform, true, true);
		refugees = camera.locate(*theRefugee, true);
		cv::imshow("Original", camera.imageOriginal);
		ss->predictionUpdate(prevLocation);
		this->location = ss->getRobotPos();
		map->platformInSight = false;
		for(int i = 0; i < platforms.size(); i++)
		{
			map->update(platforms[i], ss, &(this->location));
			this->location = ss->getRobotPos();
		}
		for(int i = 0; i < cones.size(); i++)
		{
			map->update(cones[i], ss, &(this->location));
			this->location = ss->getRobotPos();
		}
		for(int i = 0; i < refugees.size(); i++)
		{
			map->update(refugees[i], ss, &(this->location));
			this->location = ss->getRobotPos();
		}
		navi->planPath(map->ship->location, map->cones, location, 100, 0.05);
		theMap = map->draw(this->location);
		navi->drawPath(theMap, Map::Scale);
		cv::imshow("Map", theMap);
		map->manageCones(ss);
		map->manageRefugees(ss);
		this->prevLocation = this->location;
		//imshow("Map", map->draw(*(this->ss)));
		
	

		k = waitKey(1);
		switch(k)
		{
		case 1048603:
			{
				camera.stop();
				destroyWindow("Map");
				return;
			}
		case 1048686:
			{
				navi->setParameters();
				break;
			}
		case 1048691:
			{
				navi->saveParameters();
			}
		}
	}
	
}

void Robot::calibrate(void)
{
	Thing* references[] = {theCone, thePlatform, theRefugee};
	std::vector<std::vector<Scalar> > minHSVs, maxHSVs;
	std::vector<Scalar> coneMin, coneMax, platformMin, platformMax, refugeeMin, refugeeMax;
	minHSVs.push_back(coneMin);
	minHSVs.push_back(platformMin);
	minHSVs.push_back(refugeeMin);
	maxHSVs.push_back(coneMax);
	maxHSVs.push_back(platformMax);
	maxHSVs.push_back(refugeeMax);
	for(int i = 0; i < 3; i++)
	{
		minHSVs.at(i).push_back(references[i]->minHSV);
		maxHSVs.at(i).push_back(references[i]->maxHSV);
	}

	int k, iterator = 0;
	camera.start();
	while(1)
	{
		camera.capture();
		camera.calibrate(references[iterator], minHSVs.at(iterator), maxHSVs.at(iterator));
		if(minHSVs.at(iterator).size() > 0)
		{
			references[iterator]->minHSV = minHSVs.at(iterator).back();
			references[iterator]->maxHSV = maxHSVs.at(iterator).back();
		}
		else
		{
			references[iterator]->minHSV = Scalar(255, 255, 255);
			references[iterator]->maxHSV = Scalar(0, 0, 0);
		}
		k = waitKey(1);
		switch(k)
		{
		case 1113864:
			{
				if(minHSVs.at(iterator).size() > 0)
				{
					minHSVs.at(iterator).pop_back();
					maxHSVs.at(iterator).pop_back();
				}
				break;
			}
		case 1048603:
			{
				camera.stop();
				return;
			}
		case 1048676:
			{
				camera.distortionCalib(10, 8, 6);
				break;
			}
		case 1048688:
			{
				namedWindow("Camera Properties", CV_WINDOW_AUTOSIZE);
				createTrackbar("Exposure", "Camera Properties", &camera.exposure, Camera::maxexposure, onExpTrackbar, &camera);
				createTrackbar("Gain", "Camera Properties", &camera.gain, Camera::maxgain, onGainTrackbar, &camera);
				createTrackbar("Brightneess", "Camera Properties", &camera.brightness, Camera::maxbrightness, onBriTrackbar, &camera);
				createTrackbar("Contranst", "Camera Properties", &camera.contrast, Camera::maxcontrast, onContTrackbar, &camera);
				createTrackbar("Saturation", "Camera Properties", &camera.saturation, Camera::maxsaturation, onSatTrackbar, &camera);
				break;
			}
		case 1048690:
			{
				camera.rescale(theCone);
				break;
			}
		case 1048691:
			{
				writeCalibFile(references, 3);
				std::cout << "calibration saved \n";
				break;
			}
		case 1113938:
			{
				iterator++;
				iterator = iterator%3;
				break;
			}
		case 1113940:
			{
				iterator--;
				iterator = (3+iterator)%3;
				break;
			}
		}
	}
}

void Robot::setROIs(void)
{
	double coneMin = 140, coneMax = 300, overlap = 20;
	double cameraMin, cameraMax, x, w, shipMin, shipMax;
	
	theCone->ROI.height = camera.frameHeight;
	thePlatform->ROI.height = camera.frameHeight;
	theRefugee->ROI.height = camera.frameHeight;

	cameraMin = location.x - camera.FrameWidth/(2*Camera::Scale);
	cameraMax = location.x + camera.FrameWidth/(2*Camera::Scale);
	shipMin = map->ship->location.x - map->ship->length/2;
	shipMax = map->ship->location.x + map->ship->length/2;

	x = MAX(coneMin, cameraMin);
	x = (x - (location.x - camera.FrameWidth/(2*Camera::Scale)))*Camera::Scale;

	w = MIN(coneMax, cameraMax);
	w = (w - (location.x - camera.FrameWidth/(2*Camera::Scale)))*Camera::Scale;

	theCone->ROI.x = MAX((int)x, 0);
	theCone->ROI.width = MIN((int)(w - x), (int)camera.FrameWidth);
	thePlatform->ROI.x = theCone->ROI.x == 0 ? MAX(theCone->ROI.width-overlap, 0) : 0;
	thePlatform->ROI.width = MIN(camera.FrameWidth - theCone->ROI.width + overlap, (int)camera.FrameWidth);
	
	x = MAX(shipMin, cameraMin);
	x = (x - (location.x - camera.FrameWidth/(2*Camera::Scale)))*Camera::Scale;

	w = MIN(shipMax + overlap, cameraMax);
	w = (w - (location.x - camera.FrameWidth/(2*Camera::Scale)))*Camera::Scale;

	theRefugee->ROI.x = MAX((int)x, 0);
	theRefugee->ROI.width = MIN((int)(w-x), (int)camera.FrameWidth);
}

void Robot::test(void)
{
    int k;
    this->connectToSTM();
    char data[] = {0xFA, 0x61, 0x31, 0x73, 0x00, 0x00, 0x00, 0x00};
    char recvBuffer[8];
    char recv;
    cv::Point armPos;
    camera.start();
    std::vector<Cone> cones;
    std::vector<Refugee> refugees;
    this->location = cv::Point(Map::Height/2, Map::Width/2);
    cv::Mat theMap;
    for(int i = 0; i < 8; recvBuffer[i++] = 0);
    while(1)
    {
        my_serial_stream >> recv;
        if(recv == 0xFA)
        {
            for(int i = 0; i < 8; i++)
            {
                my_serial_stream >> recvBuffer[i];
            }
            updateArmAngles(recvBuffer);
        }
        armPos = arm1->getPosition();
        std::cout << armPos << std::endl;
        camera.capture();
        cones = camera.locate(*theCone, true);
        refugees = camera.locate(*theRefugee, true);
        cv::imshow("Original", camera.imageOriginal);
        ss->predictionUpdate(prevLocation);
        this->location = ss->getRobotPos();
        map->platformInSight = false;
        for(int i = 0; i < cones.size(); i++)
        {
                map->update(cones[i], ss, &(this->location));
                this->location = ss->getRobotPos();
        }
        for(int i = 0; i < refugees.size(); i++)
        {
                map->update(refugees[i], ss, &(this->location));
                this->location = ss->getRobotPos();
        }
        navi->planPath(map->ship->location, map->cones, location + armPos, 100, 0.05);
        theMap = map->draw(this->location);
        navi->drawPath(theMap, Map::Scale);
        cv::imshow("Map", theMap);
        map->manageCones(ss);
        map->manageRefugees(ss);
        this->prevLocation = this->location;
        k = cv::waitKey(1);
        switch(k)
        {
            case 1048603:
                {
                    camera.stop();
                    return;
                }
        }
    }
    
    //my_serial_stream << data;
    
    
	
}

void Robot::update(void)
{
	this->location += this->location - this->prevLocation;
}

void Robot::updateArmAngles(char* recvBuffer)
{
    int temp;
    double angles[3];
    for(int i = 0; i < 3; i++)
    {
        temp = ((recvBuffer[i]  & 0xF0)<< 8) | recvBuffer[i+1] & 0x0F;
        angles[i] = temp*M_2_PI/1200;
    }
    this->arm1->updateAngles(angles[0], angles[1]);
    this->arm2->updateAngles(angles[0] + M_PI_2, angles[2]);
}

bool Robot::init(void)
{
	Thing* references[] = {theCone, thePlatform, theRefugee};
	std::string line;
	std::ifstream calibFile;
	std::vector<std::string> v;
	calibFile.open("CalibFile.txt");
	if(!calibFile.is_open())
	{
		std::cout << "couldn't open calibFile";
		return false;
	}
	while(std::getline(calibFile, line))
	{
		v = split(line);
		if(v[0] == "CameraScale")
			Camera::Scale = std::stod(v[1]);
		if(v[0] == "CameraGain")
			camera.gain = std::stod(v[1]);
		for(int i = 0; i < 3; i++)
		{
			if(v[0] == references[i]->toString())
			{
				for(int j = 0; j < 3; j++)
				{
					std::getline(calibFile, line);
					v = split(line);
					references[i]->minHSV[j] = std::stod(v[0]);
					references[i]->maxHSV[j] = std::stod(v[1]);
				}
			}
		}
	}
	calibFile.close();
	navi->readParameters();
	return true;
}

void Robot::reset(void)
{
	location = map->home->location;
	map->reset();
	ss->reset(location, map->home->location, map->ship->location);
}

void Robot::writeCalibFile(Thing** things, int nrOfThings)
{
	std::stringstream sstm;
	std::ofstream calibFile;
	calibFile.open("CalibFile.txt");
	if(!calibFile.is_open())
	{
		std::cout << "couldn't open calibFile";
		return;
	}
	sstm << "CameraScale \t" << Camera::Scale << std::endl;  
	sstm << "CameraGain \t" << camera.gain << std::endl;  
	for(int i = 0; i < nrOfThings; i++)
	{
		sstm << things[i]->toString() << "\n";
		for(int j = 0; j < 3; j++)
		{
			sstm << "\t" << things[i]->minHSV[j] << "\t" << things[i]->maxHSV[j] << "\n";
		}
	}
	calibFile << sstm.str();
	calibFile.close();
}

void Robot::connectToSTM(void)
{
    if(!my_serial_stream.IsOpen())
    {
        my_serial_stream.Open("/dev/ttyACM0");
    }
}

void Robot::calibrateMotors(void)
{
    this->connectToSTM();
    int multiplier = 0;
    unsigned char data[] = {0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    std::string line;
    std::vector<std::string> v;
    std::ifstream calibFile;
    calibFile.open("MotorCalibFile.txt");
    if(!calibFile.is_open())
    {
        std::cout << "couldn't open MotorCalibFile" << std::endl;
        return;
    }
    while(std::getline(calibFile, line))
    {
        v = split(line);
        switch(v[0])
        {
            case "M1P_current":
                data[1] = 1;
                data[2] = 'p';
                data[3] = 'c';
                multiplier = Robot::pMultiplier;
                break;
            case "M1P_angle":
                data[1] = 1;
                data[2] = 'p';
                data[3] = 'a';
                multiplier = Robot::pMultiplier;
                break;
            case "M1P_speed":
                data[1] = 1;
                data[2] = 'p';
                data[3] = 's';
                multiplier = Robot::pMultiplier;
                break;
            case "M1P_Iangle":
                data[1] = 1;
                data[2] = 'p';
                data[3] = 'I';
                multiplier = Robot::IMultiplier;
                break;
            case "M1S_current":
                data[1] = 1;
                data[2] = 's';
                data[3] = 'c';
                multiplier = Robot::pMultiplier;
                break;
            case "M1S_angle":
                data[1] = 1;
                data[2] = 's';
                data[3] = 'a';
                multiplier = Robot::pMultiplier;
                break;
            case "M1S_speed":
                data[1] = 1;
                data[2] = 's';
                data[3] = 's';
                multiplier = Robot::pMultiplier;
                break;
            case "M1S_Ispeed":
                data[1] = 1;
                data[2] = 's';
                data[3] = 'I';
                multiplier = Robot::IMultiplier;
                break;
            case "M2P_current":
                data[1] = 2;
                data[2] = 'p';
                data[3] = 'c';
                multiplier = Robot::pMultiplier;
                break;
            case "M2P_angle":
                data[1] = 2;
                data[2] = 'p';
                data[3] = 'a';
                multiplier = Robot::pMultiplier;
                break;
            case "M2P_speed":
                data[1] = 2;
                data[2] = 'p';
                data[3] = 's';
                multiplier = Robot::pMultiplier;
                break;
            case "M2P_Iangle":
                data[1] = 2;
                data[2] = 'p';
                data[3] = 'I';
                multiplier = Robot::IMultiplier;
                break;
            case "M2S_current":
                data[1] = 2;
                data[2] = 's';
                data[3] = 'c';
                multiplier = Robot::pMultiplier;
                break;
            case "M2S_angle":
                data[1] = 2;
                data[2] = 's';
                data[3] = 'a';
                multiplier = Robot::pMultiplier;
                break;
            case "M2S_speed":
                data[1] = 2;
                data[2] = 's';
                data[3] = 's';
                multiplier = Robot::pMultiplier;
                break;
            case "M2S_Ispeed":
                data[1] = 2;
                data[2] = 's';
                data[3] = 'I';
                multiplier = Robot::IMultiplier;
                break;
            case "M3P_current":
                data[1] = 3;
                data[2] = 'p';
                data[3] = 'c';
                multiplier = Robot::pMultiplier;
                break;
            case "M3P_angle":
                data[1] = 3;
                data[2] = 'p';
                data[3] = 'a';
                multiplier = Robot::pMultiplier;
                break;
            case "M3P_speed":
                data[1] = 3;
                data[2] = 'p';
                data[3] = 's';
                multiplier = Robot::pMultiplier;
                break;
            case "M3P_Iangle":
                data[1] = 3;
                data[2] = 'p';
                data[3] = 'I';
                multiplier = Robot::IMultiplier;
                break;
            case "M3S_current":
                data[1] = 3;
                data[2] = 's';
                data[3] = 'c';
                multiplier = Robot::pMultiplier;
                break;
            case "M3S_angle":
                data[1] = 3;
                data[2] = 's';
                data[3] = 'a';
                multiplier = Robot::pMultiplier;
                break;
            case "M3S_speed":
                data[1] = 3;
                data[2] = 's';
                data[3] = 's';
                multiplier = Robot::pMultiplier;
                break;
            case "M3S_Ispeed":
                data[1] = 3;
                data[2] = 's';
                data[3] = 'I';
                multiplier = Robot::IMultiplier;
                break;
            case "M4P_current":
                data[1] = 4;
                data[2] = 'p';
                data[3] = 'c';
                multiplier = Robot::pMultiplier;
                break;
            case "M4P_angle":
                data[1] = 4;
                data[2] = 'p';
                data[3] = 'a';
                multiplier = Robot::pMultiplier;
                break;
            case "M4P_speed":
                data[1] = 4;
                data[2] = 'p';
                data[3] = 's';
                multiplier = Robot::pMultiplier;
                break;
            case "M4P_Iangle":
                data[1] = 4;
                data[2] = 'p';
                data[3] = 'I';
                multiplier = Robot::IMultiplier;
                break;
            case "M4S_current":
                data[1] = 4;
                data[2] = 's';
                data[3] = 'c';
                multiplier = Robot::pMultiplier;
                break;
            case "M4S_angle":
                data[1] = 4;
                data[2] = 's';
                data[3] = 'a';
                multiplier = Robot::pMultiplier;
                break;
            case "M4S_speed":
                data[1] = 4;
                data[2] = 'a';
                data[3] = 's';
                multiplier = Robot::pMultiplier;
                break;
            case "M4S_Ispeed":
                data[1] = 4;
                data[2] = 's';
                data[3] = 'I';
                multiplier = Robot::IMultiplier;
                break;
        }
        data[4] = ((v[1]*multiplier) & 0xFF00) >> 8;
        data[5] = (v[1]*multiplier) & 0x00FF;
        my_serial_stream << data;
    }        
    
}

Robot::~Robot(void)
{
}


void onExpTrackbar(int i, void *param)
{
	Camera *c = (Camera*)param;
	c->cap.set(CV_CAP_PROP_EXPOSURE, c->exposure -9);
}
void onSatTrackbar(int i, void* param)
{
	Camera *c = (Camera*)param;
	c->cap.set(CV_CAP_PROP_SATURATION, c->saturation);
}
void onGainTrackbar(int i, void* param)
{
	Camera *c = (Camera*)param;
	c->cap.set(CV_CAP_PROP_GAIN, c->gain);
}
void onContTrackbar(int i, void* param)
{
	Camera *c = (Camera*)param;
	c->cap.set(CV_CAP_PROP_CONTRAST, c->contrast);
}
void onBriTrackbar(int i, void* param)
{
	Camera *c = (Camera*)param;
	c->cap.set(CV_CAP_PROP_BRIGHTNESS, c->brightness);
}
