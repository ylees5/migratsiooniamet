
#include "Navigator.h"

using namespace Eigen;
using namespace cv;

const int Navigator::maxAttExp = 5;
const int Navigator::maxAttGain = 10;
const int Navigator::maxRepExp = 5;
const int Navigator::maxRepGain = 10;
const int Navigator::maxObsRX = 100;
const int Navigator::maxObsRY = 100;
const int Navigator::maxTargTol = 30;
const int Navigator::maxLambda = 30;
const int Navigator::maxMaxIter = 300;

const int Navigator::minAttExp = 0.1;
const int Navigator::minAttGain = 1;
const int Navigator::minRepExp = 0.1;
const int Navigator::minRepGain = 1;
const int Navigator::minObsRX = 10;
const int Navigator::minObsRY = 10;
const int Navigator::minTargTol = 1;
const int Navigator::minLambda = 1;
const int Navigator::minMaxIter = 50;

const double Navigator::stepAttExp = 0.1;
const double Navigator::stepAttGain = 1;
const double Navigator::stepRepExp = 0.1;
const double Navigator::stepRepGain = 1;
const double Navigator::stepObsRX = 1;
const double Navigator::stepObsRY = 1;
const double Navigator::stepTargTol = 1;
const double Navigator::stepLambda = 1;
const double Navigator::stepMaxIter = 1;


Navigator::Navigator(void)
{
	attractionExponent = 1;
	attractionGain = 1;
	repulsionExponent = 1;
	repulsionGain = 1;
	obstacleRX = 60;
	obstacleRY = 30;
	lambda = 10;
	maxIterations = 100;
	targetTolerance = 20;
}

Vector2d Navigator::getAttractionForce(Point target, Point robot)
{
	Vector2d ret(0,0);
	double x, y, dx, dy, exp, gain;
	dx = robot.x - target.x;
	dy = robot.y - target.y;
	exp = attractionExponent*stepAttExp + minAttExp;
	gain = attractionGain*stepAttGain + minAttGain;
	x = -2*exp*gain*dx*std::pow(dx*dx + dy*dy, exp - 1);
	//x = lambda*x/std::pow(lambda + x, 2);
	y = -2*exp*gain*dy*std::pow(dx*dx + dy*dy, exp - 1);
	//y = lambda*y/std::pow(lambda + y, 2);
	ret << x, y;
	return ret;
}

Vector2d Navigator::getRepulsionForce(cv::Point obstacle, Point robot)
{
	Vector2d ret(0, 0);
	double x, y, dx, dy, rx, ry, exp, gain;
	dx = robot.x - obstacle.x;
	dy = robot.y - obstacle.y;
	rx = obstacleRX*stepObsRX + minObsRX;
	ry = obstacleRY*stepObsRY + minObsRY;
	exp = repulsionExponent*stepRepExp + minRepExp;
	gain = repulsionGain*stepRepGain + minRepGain;
	x = 2*exp*gain*dx/(rx*rx)*std::pow((std::pow(dx/rx, 2) + std::pow(dy/ry, 2)), -exp - 1);
	//x = lambda*x/std::pow(lambda + x, 2);
	y = 2*exp*gain*dy/(ry*ry)*std::pow((std::pow(dx/rx, 2) + std::pow(dy/ry, 2)), -exp - 1);
	//y = lambda*y/std::pow(lambda + y, 2);
	ret(0) = x;
	ret(1) = y;
	return ret;
}

Vector2d Navigator::getResultantForce(Point target, std::vector<Cone> obstacles, Point robot)
{
	Vector2d ret(0, 0);
	std::vector<Cone>::iterator it = obstacles.begin();
	ret += getAttractionForce(target, robot);
	while(it != obstacles.end())
	{
		ret += getRepulsionForce(it->location, robot);
		++it;
	}
	return ret/ret.norm();
}

bool Navigator::planPath(Point target, std::vector<Cone> &obstacles, Point robot, double speed, double dt)
{
	Point nextPos = robot;
	Vector2d dir;
	double targetDist = distance(target, nextPos);
	double tol = targetTolerance*stepTargTol + minTargTol;
	double maxiter = maxIterations*stepMaxIter + minMaxIter;
	int n = 0;
	path.clear();
	path.push_back(nextPos);
	while(targetDist > tol && n <= maxiter)
	{
		dir = getResultantForce(target, obstacles, nextPos);
		nextPos.x += dir(0)*speed*dt;
		nextPos.y += dir(1)*speed*dt;
		path.push_back(nextPos);
		targetDist = distance(target, nextPos);
		n++;
	}
	return n > maxIterations ? false : true;
}

void Navigator::drawPath(cv::Mat &image, double scale)
{
	std::vector<Point>::iterator it1 = path.begin();
	std::vector<Point>::iterator it2 = path.begin();
	if(it1 != path.end())
		++it2;
	while(it2 != path.end())
	{
		line(image, *it1*scale, *it2*scale, Scalar(0, 255, 255), 2);
		circle(image, *it2*scale, 5, Scalar(0, 255, 0), 1);
		++it1;
		++it2;
	}
}

void Navigator::setParameters(void)
{
	namedWindow("Navigator paramaters", CV_WINDOW_AUTOSIZE);
	createTrackbar("attraction Exponent", "Navigator paramaters", &attractionExponent, (maxAttExp - minAttExp)/stepAttExp);
	createTrackbar("attraction Gain", "Navigator paramaters", &attractionGain, (maxAttGain - minAttGain)/stepAttGain);
	createTrackbar("repulsion Exponent", "Navigator paramaters", &repulsionExponent, (maxRepExp - minRepExp)/stepRepExp);
	createTrackbar("repulsion Gain", "Navigator paramaters", &repulsionGain, (maxRepGain - minRepGain)/stepRepGain);
	createTrackbar("ObstacleRX", "Navigator paramaters", &obstacleRX, (maxObsRX - minObsRX)/stepObsRX);
	createTrackbar("ObstacleRY", "Navigator paramaters", &obstacleRY, (maxObsRY - minObsRY)/stepObsRY);
	createTrackbar("target Tolerance", "Navigator paramaters", &targetTolerance, (maxTargTol - minTargTol)/stepTargTol);
	createTrackbar("Max iterations", "Navigator paramaters", &maxIterations, (maxMaxIter - minMaxIter)/stepMaxIter);
}

void Navigator::saveParameters(void)
{
	std::ofstream file;
	std::stringstream sst;
	file.open("NavigatorParams.txt");
	if(!file.is_open())
	{
		std::cout << "Couldn't open navigatorParameters file";
		return;
	}
	sst << "AttractionExponent \t" << attractionExponent << std::endl;
	sst << "AttractionGain \t" << attractionGain << std::endl;
	sst << "RepulsionExponent \t" << repulsionExponent << std::endl;
	sst << "RepulsionGain \t" << repulsionGain << std::endl;
	sst << "ObstacleRX \t" << obstacleRX << std::endl;
	sst << "ObstacleRY\t" << obstacleRY << std::endl;
	sst << "TargetTolerance \t" << targetTolerance << std::endl;
	sst << "MaxIterations \t" << maxIterations << std::endl;

	file << sst.str();
	file.close();
}

void Navigator::readParameters(void)
{
	std::string line;
	std::ifstream file;
	std::vector<std::string> v;
	file.open("NavigatorParams.txt");
	if(!file.is_open())
	{
		std::cout << "Couldn't open navigatorParameters file";
		return;
	}
	while(std::getline(file, line))
	{
		v = split(line);
		if(v[0] == "AttractionExponent")
			attractionExponent = std::stod(v[1]);
		if(v[0] == "AttractionGain")
			attractionGain = std::stod(v[1]);
		if(v[0] == "RepulsionExponent")
			repulsionExponent = std::stod(v[1]);
		if(v[0] == "RepulsionGain")
			repulsionGain = std::stod(v[1]);
		if(v[0] == "ObstacleRX")
			obstacleRX = std::stod(v[1]);
		if(v[0] == "ObstacleRY")
			obstacleRY = std::stod(v[1]);
		if(v[0] == "TargetTolerance")
			targetTolerance = std::stod(v[1]);
		if(v[0] == "MaxIterations")
			maxIterations = std::stod(v[1]);
	}
	std::cout << "navigation params inited" << std::endl;
	file.close();
}

Navigator::~Navigator(void)
{
}


double distance(Point p1, Point p2)
{
	Point diff = p1 - p2;
	double dist = std::pow((double)diff.x, 2) + std::pow((double)diff.y, 2);
	dist = std::pow(dist, 0.5);
	return dist;
}

std::vector<std::string> split(const std::string& s)
{
	std::vector<std::string> ret;
	typedef std::string::size_type string_size;
	string_size i = 0;
	while(i != s.size())
	{
		while(i != s.size() && isspace(s[i]))
			++i;
		string_size j = i;
		while(j != s.size() && !isspace(s[j]))
			++j;
		if(i != j)
		{
			ret.push_back(s.substr(i, j - i));
			i = j;
		}
	}
	return ret;
}
