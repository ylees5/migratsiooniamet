
#include "GUI.h"
#include <SerialStream.h>

using namespace cv;

GUI::GUI(void)
{
	robot = new Robot();
	namedWindow("GUI", CV_WINDOW_AUTOSIZE);
	//cvCreateButton("Calibrate");


}

void GUI::execute(void)
{
    int keyPressed;
    while(1)
    {
        keyPressed = waitKey(0);

        switch(keyPressed)
        {
        case 1048603:   // "ESC" pressed
            return;
        case 1048675:   //"C" pressed
        {
            robot->calibrate();
            break;
        }
        case 1048676:   //"D" pressed
        {
            robot->camera.start();

            robot->camera.distortionCalib(10, 8, 6);
            break;
        }
        case 1048690:   //"R" pressed
        {
            robot->reset();
            std::cout << "reset done \n";
            break;
        }
        case 1048692:   //"T" pressed
        {
            robot->test();
            break;
        }
        case 1048681:   //"I" pressed
        {
            if(robot->init())
                std::cout << "init succeedd \n";
            else
                std::cout << "init failed \n";
            break;
        }
        case 1048691:   //"S" pressed
        {
            robot->start();
            break;
        }
        case 1048685:   //"M" presseed
        {
            LibSerial::SerialStream my_serial_stream;
            unsigned char recv;
            my_serial_stream.Open("/dev/ttyACM0");
            unsigned char data[] = {0xFA, 'm', '1', 'p', 0x02, 0x58, 0x00, 0x00};
            my_serial_stream << data;
            break;
        }

        default:        //other key pressed
        {
            if(keyPressed > 0)
                std::cout << keyPressed << std::endl;
        }
        }	
    }
}

GUI::~GUI(void)
{
}
