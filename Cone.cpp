#include "StdAfx.h"
#include "Cone.h"

using namespace cv;

Cone::Cone(void)
{
	this->minHSV = Scalar(179, 255, 255);
	this->maxHSV = Scalar(0, 0, 0);
	this->colour = Scalar(0, 0, 255);
	this->minSize = 50*50;
	this->maxSize = 250*250;
	this->height = 30;
	this->width = 30;
	this->length = 30;
	this->certainty = 0;
	this->updated = false;
	this->index = -1;
	this->ROI = Rect(0, 0, 0, 0);
}

Cone::Cone(Thing t)
{
	this->minHSV = t.minHSV;
	this->maxHSV = t.maxHSV;
	this->colour = t.colour;
	this->minSize = t.minSize;
	this->maxSize = t.maxSize;
	this->length = 30;
	this->width = 30;
	this->height = t.height;
	this->certainty = t.certainty;
	this->updated = false;
	this->index = t.index;
	this->ROI = t.ROI;
}

std::string Cone::toString()
{
	std::stringstream sst;
	sst << "Cone";
	return sst.str();
}

double Cone::getTrueArea()
{
	return this->width*this->length;
}


double Cone::getWidth(void)
{
	return width;
}
double Cone::getLength(void)
{
	return length;
}

Cone::~Cone(void)
{
}
