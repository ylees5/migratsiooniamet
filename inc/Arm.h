/* 
 * File:   Arm.h
 * Author: icd
 *
 * Created on November 3, 2015, 9:32 AM
 */

#pragma once

#include <cmath>
#include "opencv2/opencv.hpp"

class Arm {
public:
    double l1, l2;
    double angles[2];
    
    Arm(void);
    Arm(double len1, double len2);
    cv::Point getPosition(void);
    void updateAngles(double a1, double a2);
    ~Arm(void);
    
    
};



