#pragma once

#include "Map.h"
#include <fstream>
#include <iostream>
#include <cctype>
#include <SerialStream.h>
#include <math.h>
#include "opencv2/opencv.hpp"
#include "StateSpace.h"
#include "Camera.h"
#include "Cone.h"
#include "Platform.h"
#include "Refugee.h"
#include "Navigator.h"
#include "Arm.h"



class Robot
{
public:
	StateSpace *ss;
	Camera camera;
	cv::Point location;
	cv::Point prevLocation;
	Map *map;
	Cone *theCone;
	Platform *thePlatform;
	Refugee *theRefugee;
	Navigator *navi;
	Arm *arm1;
        Arm *arm2;
	

	Robot(void);
	void start(void);
	void calibrate(void);
	void setROIs(void);
	void test(void);
	void update(void);
        void updateArmAngles(char* recvBuffer);
	bool init(void);
	void reset(void);
	void writeCalibFile(Thing** things, int nrOfThings);
	~Robot(void);  
};



void onExpTrackbar(int i, void* param);
void onSatTrackbar(int i, void* param);
void onGainTrackbar(int i, void* param);
void onContTrackbar(int i, void* param);
void onBriTrackbar(int i, void* param);
