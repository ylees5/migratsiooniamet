#pragma once

#include <Eigen/Dense>
#include "opencv2/opencv.hpp"
#include "Camera.h"


class StateSpace
{
public:

	static const double k;
	static const double minR;
	static const double maxR;
	static const double chiSquaredVal;
	static const int MaxSize;
	static const int RobotPoses;
	static const int LandmarkPoses;

	Eigen::MatrixXd P, K, H, R, Q, Fu, Fy, IN, Jxr, Jz;
	Eigen::VectorXd y, predZ, Z, in;;
	
	int size;

	StateSpace(void);
	StateSpace(cv::Point location, cv::Point home, cv::Point ship);
	void predictionUpdate(cv::Point prevPos);
	void measurementUpdate(Thing t);
	void localize(Thing t);
	void map(Thing t);
	void predictObservations(int index);
	void addMeasurements(Thing t);
	void addNewLandmark(Thing t);
	void removeLandmark(int index);
	cv::Point getLandmarkPos(int index);
	cv::Point getRobotPos(void);
	void Init(void);
	void reset(cv::Point location, cv::Point home, cv::Point ship);
	cv::RotatedRect getErrorEllipse(int index);
	~StateSpace(void);
};

