#pragma once


#include "Cone.h"
#include "Refugee.h"
#include "Platform.h"
#include <vector>
#include "Camera.h"
#include "StateSpace.h"



class Map
{
public:

	static const double Scale;
	static const int Width;
	static const int Height;

	std::vector<Cone> cones;
	std::vector<Refugee> refugees;
	Platform *home;
	Platform *ship;
	int nrOfLandmarks;
	int nrOfCones;
	bool completed;
	bool allConesSeen;
	bool platformInSight;

	Map(void);
	void update(Cone c, StateSpace *ss, cv::Point *robot);
	void update(Platform p, StateSpace *ss, cv::Point *robot);
	void update(Refugee r, StateSpace *ss, cv::Point *robot);
	cv::Mat draw(cv::Point robot);
	cv::Mat draw(StateSpace ss);
	void clearFalseCones(void);
	void manageCones(StateSpace *ss);
	void manageRefugees(StateSpace *ss);
	void reset(void);
	~Map(void);
};

bool isOnPlatform(Platform platform, cv::Point p);
bool areSameCones(Cone c1, Cone c2);
bool areSameRefugees(Refugee r1, Refugee r2);
void drawRect(cv::Mat &image, cv::Point center,int height, int width, cv::Scalar color, int thickness = 1, int lineType = 8);
