#pragma once
#include "Thing.h"
class Cone :
	public Thing
{
public:

	double width;
	double length;

	Cone(void);
	Cone(Thing t);
	std::string toString();
	double getTrueArea();
	double getWidth();
	double getHeight();
	double getLength();
	~Cone(void);
};

