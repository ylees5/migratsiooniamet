#pragma once

#include "opencv2/opencv.hpp"


class Thing
{
public:

	cv::Scalar minHSV;
	cv::Scalar maxHSV;
	cv::Scalar colour;
	cv::Point2i location;
	double height;
	double minSize;
	double maxSize;
	double area;
	double certainty;
	bool updated;
	int index;
	double inSight;
	cv::Rect ROI;

	Thing(void);
	virtual std::string toString();
	virtual double getTrueArea();
	virtual double getWidth();
	double getHeight();
	virtual double getLength();
	~Thing(void);
};

