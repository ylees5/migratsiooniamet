#include "StdAfx.h"
#include "Refugee.h"

using namespace cv;

Refugee::Refugee(void)
{
	this->minHSV = Scalar(179, 255, 255);
	this->maxHSV = Scalar(0, 0, 0);
	this->colour = Scalar(255, 0, 0);
	this->minSize = 50*50;
	this->maxSize = 80*80;
	this->diameter = 17;
	this->height = 5;
	this->certainty = 0;
	this->updated = false;
	this->index = -1;
	this->ROI = Rect(0, 0, 0, 0);
}

Refugee::Refugee(Thing t)
{
	this->minHSV = t.minHSV;
	this->maxHSV = t.maxHSV;
	this->colour = t.colour;
	this->minSize = t.minSize;
	this->maxSize = t.maxSize;
	this->diameter = 17;
	this->height = t.height;
	this->certainty = t.certainty;
	this->index = t.index;
	this->ROI = t.ROI;
	this->updated = t.updated;
}

std::string Refugee::toString()
{
	return "Refugee";
}

double Refugee::getTrueArea()
{
	return pow(this->diameter/2, 2) * CV_PI;
}


double Refugee::getWidth(void)
{
	return diameter;
}

double Refugee::getLength(void)
{
	return diameter;
}

Refugee::~Refugee(void)
{
}
